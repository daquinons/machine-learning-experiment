from sklearn import svm, preprocessing
import pandas as pd
import data_management
from test import test, report

def train(X, y, test_size):
    X = preprocessing.scale(X)
    clf = svm.SVC(kernel="linear", C= 1.0)
    number_test_samples = int(len(y) * test_size)
    print("Training data...")
    clf.fit(X[:-number_test_samples],y[:-number_test_samples])

    return clf, X, y


if __name__ == '__main__':
    ## Data
    pair = "USDCHF"
    interval = "H1"
    offset_period = 1
    features_list = ["ema_difference", "rsi", "momentum"]
    price_type = "average"
    test_size=0.20

    # Building dataset
    X, y = data_management.build_dataset(pair, interval, offset_period, features_list, price_type)

    # Training
    clf, X, y = train(X, y, test_size)

    # Testing
    accuracy = test(clf, X, y, test_size)
    report(pair, interval, features_list, accuracy)
