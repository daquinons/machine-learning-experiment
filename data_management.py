import os
import pandas as pd
from numpy import nan as numpy_nan
import numpy as np
from datetime import datetime
import time
import features

def get_filepaths_for_symbol(symbol, file_format="csv"):
    files_to_return = []
    working_directory = os.getcwd()
    data_path = working_directory + "/../data/" + symbol
    for each_file in os.listdir(data_path):
        if each_file.endswith("." + file_format):
            files_to_return.append(data_path + "/" + each_file)

    return files_to_return

def get_csv_file_path_for_symbol(symbol, timeperiod):
    csv_files = get_filepaths_for_symbol(symbol)
    for each_file in csv_files:
        timeperiod_to_check = each_file.split("_")[1]
        if timeperiod in timeperiod_to_check:
            return each_file

def get_data_from_csv_files_for_timeperiod(symbol, timeperiod):
    csv_file = get_csv_file_path_for_symbol(symbol, timeperiod)
    data = get_data_from_csv_file(csv_file)

    return data


def get_data_from_csv_file(fullpath):
    data = pd.read_csv(filepath_or_buffer=fullpath, index_col=["datetime"], parse_dates=["datetime"])
    # Average for buy/sell prices
    data["close_average"] = pd.concat((data["close_ask"], data["close_bid"]), axis=1).mean(axis=1)
    return data

def label_data(pandas_data, offset_period):
    price_change = []
    close_price_values = pandas_data["price_data"]
    for each_number in range(0, len(close_price_values)):
        current_price = close_price_values[each_number]
        try:
            offset_price = close_price_values[each_number+offset_period]
            if offset_price < current_price:
                price_change.append("negative")
            elif offset_price > current_price:
                price_change.append("positive")
            else:
                price_change.append(numpy_nan)
        except IndexError:
            price_change.append(numpy_nan)

    pandas_data["price_change_offset_" + str(offset_period)] = pd.Series(price_change, index=pandas_data.index)

    return pandas_data

def clean_nan_data(pandas_data):
    return pandas_data.dropna()

def build_dataset(pair, interval, offset_period, features_list, price_type):
    whole_data = get_data_from_csv_files_for_timeperiod(pair, interval)
    if price_type is "average":
        close_price_data = whole_data["close_average"]
    elif price_type is "bid":
        close_price_data = whole_data["close_bid"]
    elif price_type is "ask":
        close_price_data = whole_data["close_ask"]
    # Calculating features
    print("Calculating features...")
    feature_results = {}
    for each_feature_string in features_list:
        feature_results[each_feature_string] = getattr(features, each_feature_string)(close_price_data)
    df_data = feature_results
    df_data["price_data"] = close_price_data
    df = pd.DataFrame(data=df_data, index=whole_data.index)

    # Labeling data
    print("Labeling data...")
    labeled_data = label_data(df, offset_period)
    cleaned_labeled_data = clean_nan_data(labeled_data)
    # Shuffling data
    #cleaned_labeled_data = cleaned_labeled_data.reindex(np.random.permutation(cleaned_labeled_data.index))

    # Formatting as X and y
    X = np.array(cleaned_labeled_data[features_list].values)
    y = (cleaned_labeled_data[cleaned_labeled_data.columns[-1]]
         .replace("negative",0)
         .replace("positive",1)
         .values.tolist())

    return X, y


if __name__ == '__main__':
    # Resample data to different time periods
    #resample_data_ohlc("USDCHF", "15Min")
    #get_data_from_csv_files_for_timeperiod("EURUSD", "H1")
    X, y = build_dataset("EURUSD", "H1", 1, ["ema_difference", "rsi", "momentum", "linear_regression"], "average")
    print(X)
    print(y)
