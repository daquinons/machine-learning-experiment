import time
import pandas as pd
from numpy import nan as numpy_nan
import talib

def ema_difference(pandas_data):
    feature_values = []
    short_ema = talib.EMA(pandas_data.values, timeperiod=5)
    long_ema = talib.EMA(pandas_data.values, timeperiod=10)

    for each_number in range(0, len(short_ema)):
        each_short_ema = short_ema[each_number]
        each_long_ema = long_ema[each_number]

        if not pd.isnull(each_short_ema) and not pd.isnull(each_long_ema):
            value = (each_long_ema - each_short_ema)
            feature_values.append(value)
        else:
            feature_values.append(numpy_nan)

    return feature_values

def rsi(pandas_data):
    rsi = talib.RSI(pandas_data.values, timeperiod=10)
    feature_values = rsi.tolist()

    return feature_values

def momentum(pandas_data):
    momentum = talib.MOM(pandas_data.values, timeperiod=4)
    feature_values = momentum.tolist()

    return feature_values

def linear_regression(pandas_data):
    linear_reg = talib.LINEARREG(pandas_data.values, timeperiod=11)
    feature_values = linear_reg.tolist()

    return feature_values
