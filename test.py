def test(clf, X, y, test_size):
    number_test_samples = int(len(y) * test_size)
    correct_count = 0
    print("Testing data...")
    for x in range(1, number_test_samples+1):
        x_predict = X[-x]
        x_predict = x_predict.reshape([1,-1])
        prediction = clf.predict(x_predict)
        if prediction[0] == y[-x]:
            correct_count += 1
    accuracy = (correct_count/number_test_samples)

    return accuracy

def report(pair, interval, features_list, accuracy):
    print("----- Report ------")
    print("Pair:", pair)
    print("Interval:", interval)
    print("Features:", features_list)
    print("---------------------")
    print("Accuracy:", accuracy * 100.00)
    print("---------------------")
